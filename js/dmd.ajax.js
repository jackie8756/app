function getDmdCountAjax(classfy) {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getDmdCount/",	
		data: {
			"classfy": classfy
		},
		complete: function() {},
		success: function(msg) {
			dmdCount = msg;
		},
		failed: function(msg) {
			console.log("failed");
		}		
	});
}

function getDmdAjax(classfy) {
	document.getElementById('hide-box').style.display = "none";
	document.getElementById('load_model').style.display = "block";
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getDmd/" + ajaxCount,
		data: {
			"classfy": classfy
		},
		complete: function() {},
		success: function(msg) {
			var data = msg;
			data = data.ajaxDmd;
			mui.each(data, function(i, dmd) {
				
				acc = dmd.dAccount;
				var ul = document.getElementsByClassName("mui-table-view mui-table-view-chevron");
				var li = document.createElement('li');
				usrname = convertString(dmd.username);
				li.className = 'mui-table-view-cell';
				li.innerHTML = '<div onclick="openClickedDmdPage('+dmd.id+');" class="mui-slider-cell">' + '<div class="oa-contact-cell mui-table">' +
					'<div class="oa-contact-avatar mui-table-cell">' + '<img src="' + prex + '/resources/images/stuHead/' + dmd.userHead + '" width="60" height="60" />' + 
					'<span style="white-space: nowrap">' + usrname +'</span>' +
					'</div>' +
					'<div class="oa-contact-content">' +
					'<div class="mui-clearfix">' +
					'<h4 class="tname">' + dmd.title + '</h4>' +
					'<span class="oa-contact-position-index mui-h6" style="text-align: left; color:#ffb200; font-weight: bold;">' + '悬赏:' +  dmd.price + '元</span>' +
					'</div>' +
					'<p class="oa-contact-email mui-h6">' +
					dmd.detail + 
					'</p>' + 
					'<p class="mui-h7">' +
//					'回复数:<a class="hot_tag">' + dmd.numOfReply + '</a>' + '&nbsp;&nbsp;' +					
					'</p>' +
					'</div>' +
					'</div>' +
					'</div>';
				ul[0].appendChild(li);
			});
			ajaxCount = ajaxCount + 8; //一次加载8个
			document.getElementById('load_model').style.display = "none";
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

function convertString (s){
	var realLen = getRealLength(s);
	console.log("length:"+realLen);
	if (realLen>10){
		ss = s.substr(0,8) + "...";
	}else{
		ss = s;
	}
	return ss;
}

function getRealLength(str){
	  ///<summary>获得字符串实际长度，中文2，英文1</summary>
	  ///<param name="str">要获得长度的字符串</param>
	  var realLength = 0, len = str.length, charCode = -1;
	  for (var i = 0; i < len; i++) {
	    charCode = str.charCodeAt(i);
	    if (charCode >= 0 && charCode <= 128) realLength += 1;
	    else realLength += 2;
	  }
	  return realLength;
}
