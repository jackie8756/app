/**
 * 判断是否登录状态，用于对登录成功之后的每个界面进行是否登录保护
 * 思路：
 * 1.读取本地缓存信息sessionid,若无，则没有登录
 * 2.读取到sessionId之后，读取appAccountId传入后台进行相应的登录检查
 * 以上检查通过则为登录.
 * 注：每次重新登录之后 sessionId，telNo均需要重写，空也要必须写入
 */
function isLogin() {
	var loginInfo = null;
	var sessionId = plus.storage.getItem("sessionId");
	console.info("Local sessionId is " + sessionId);
	if (sessionId != null && sessionId != "") {
		var appAccountId = plus.storage.getItem("appAccountId");
		console.info("appAccountId:" + appAccountId);
		loginInfo = isLoginAjax(sessionId, appAccountId);
	}
	console.info("loginInfo:" + loginInfo);
	return loginInfo;
}

function isLoginAjax(sessionId, appAccountId) {
	var loginInfo;
	$.ajax({
		type: "post",
		dataType: "json",
		async: false,
		timeout:5000,
		url: prex + "/isLogin/",
		data: {
			"appAccountId": appAccountId,
			"sessionId": sessionId
		},
		complete: function() {},
		success: function(msg) {
			if (msg != null) { //防止服务器端session失效
				if (msg.loginValidStatus == 3) {
					console.info("isLogin loginValidStatus is " + msg.loginValidStatus)
					loginInfo = msg;
				}
			}
			isAjaxFinish=true;
		},
		failed: function(msg) {
			console.log("isLoginAjax:failed");
			isAjaxFinish=true;
		}
	});
	return loginInfo;
}

/**
 * 本地登录
 */
function LocalStorageLogin(telNo,passwd) {
	var loginInfo;
	console.info("set html2 LocalStorageLogin");
	$.ajax({
		type: "post",
		dataType: "json",
		async: false,
		timeout:5000,
		url: prex + "/LocalStorageLogin/",
		data: {
			"telNo": telNo,
			"passwd": passwd
		},
		complete: function() {},
		success: function(msg) {
			console.info("set html2 success");
			if (msg != null) { //防止服务器端session失效
				if (msg.loginValidStatus == 3) { //登录成功
					plus.storage.setItem("sessionId", msg.sessionId);
					console.info("set localStorage sessionId:" + msg.sessionId);
					plus.storage.setItem("appAccountId", msg.appAccountId.toString());
					console.info("set localStorage appAccountId:" + msg.appAccountId.toString());
					plus.storage.setItem("telNo", msg.telNo);
					console.info("set localStorage telNo:" + msg.telNo);
					plus.storage.setItem("password", msg.password);
					console.info("set localStorage password:" + msg.password);
				}
				console.log("msg.loginValidStatus:" + msg.loginValidStatus);
				loginInfo = msg;
				isAjaxFinish=true;
			}
		},
		failed: function(msg) {
			isAjaxFinish=true;
			alert("set html2 false");
//			console.log("LocalStorageLogin:failed");
		}
	});
	return loginInfo;
}