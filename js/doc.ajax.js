var oneActionCount = 8; //一次请求界面上显示8个
function getDocCountAjax() {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getDocCount",
		complete: function() {},
		success: function(msg) {
			docCount = msg;
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

function getDocAjax() {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getDoc/" + ajaxCount,
		data: {
			"name": "zhoujun3",
			"password": "123123",
			"oneActionCount": oneActionCount
		},
		complete: function() {},
		success: function(msg) {
			var data = msg;
			data = data.ajaxDoc;			
            var ul = document.querySelector(".mui-table-view, .mui-table-view-chevron");
			mui.each(data, function(i, doc) {
				var li = document.createElement('li');
				li.className = 'mui-table-view-cell';
				li.innerHTML = '<div onclick="openClickedDocPage('+doc.id+');" class="mui-slider-cell">' + '<div class="oa-contact-cell mui-table">' +
					'<div class="oa-contact-avatar mui-table-cell">' + '<img src="' + prex + '/resources/images/tchHead/' + doc.title + '" width="60" height="60" />' + '</div>' +
					'<div class="oa-contact-content mui-table-cell">' +
					'<div class="mui-clearfix">' +
					'<h4 class="oa-contact-name" style="width: 100%;">' + doc.docName +
					'<span class=" mui-h6" style="float: right;font-size: 14px">' +  doc.price + '  元</span>'  + '</h4>' +
					'</div>' +
					'<p class="oa-contact-email mui-h6" style="color:#2f9bd2;">' +
					doc.docArea + '-' +doc.docCollege + 
					'</p>' + 
					'<p class="mui-h6">' +
					  doc.descByTch +
					'</p>' +
					'</div>' +
					'</div>' +
					'</div>';
				ul.appendChild(li);
			});
			ajaxCount = ajaxCount + 8; //一次加载8个
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}