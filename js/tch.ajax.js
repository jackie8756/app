var oneActionCount = 8; //一次请求界面上显示8个
function getTchCountAjax() {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getTchCount/",
		complete: function() {},
		success: function(msg) {
			tchCount = msg;
			//			console.log(msg);
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

function getTchAjax() {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getTch/" + ajaxCount,
		data: {
			"oneActionCount": oneActionCount
		},
		complete: function() {},
		success: function(msg) {
			var data = msg;
			var ul = document.querySelector(".mui-table-view, .mui-table-view-chevron");
			mui.each(data, function(i, tch) {
				var li = document.createElement('li');
				li.className = 'mui-table-view-cell';
				li.innerHTML = '<div onclick="openClickedTchPage(' + tch.id + ');"  class="mui-slider-cell">' + '<div class="oa-contact-cell mui-table" style="display:block;margin:5px 0 0!important">' +
					'<div class="oa-contact-avatar mui-table-cell" style="width:20%; margin-right:5%;float:left;display:inline-block">' + '<img src="' + prex + '/resources/images/tchHead/' + tch.head + '" width="65rem" height="65rem" style="margin-top:6px;"/>' + '</div>' +
					'<div class="oa-contact-content mui-table-cell" style="float:left;display:inline-block;width:73%;" >' +
					'<div class="mui-clearfix">' +
					'<h4 class="oa-contact-name"style="width: 100%;">' + tch.realname +
					'<span class=" mui-h6" style="float: right;font-size: 14px;" >' + tch.affiliation + '</span>' + '</h4>' +
					'</div>' +
					'<p class="oa-contact-email mui-h6" style="color:#2f9bd2;">' +
					tch.major + '-' + tch.degree +
					'</p>' +
					'<p class="mui-h6">' +
					'擅长:' +
					addCoursesHtml(tch.handledCourses) +
					'</p>' +
					'</div><div class="mui-clearfix"></div>' +
					'</div>' +
					'</div>';
				ul.appendChild(li);
			});

			ajaxCount = ajaxCount + 8; //一次加载8个
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

/**
 * 用于得到前几个
 */
function getTopTchsAjax() {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getTch/" + 0,
		data: {
			"oneActionCount": oneActionCount
		},
		complete: function() {},
		success: function(msg) {
			var data = msg;
			var ul = document.querySelector(".mui-table-view, .mui-table-view-chevron");
			mui.each(data, function(i, tch) {
				var li = document.querySelector(".mui-table-view-cell");
				ul.removeChild(li); //每次都删除第一个
				li = document.createElement('li');
				li.className = 'mui-table-view-cell';
				li.innerHTML = '<div onclick="openClickedTchPage(' + tch.id + ');"  class="mui-slider-cell">' + '<div class="oa-contact-cell mui-table">' +
					'<div class="oa-contact-avatar mui-table-cell">' + '<img src="' + prex + '/resources/images/tchHead/' + tch.head + '" width="60px" height="60px" style="margin-top:15px;"/>' + '</div>' +
					'<div class="oa-contact-content mui-table-cell">' +
					'<div class="mui-clearfix">' +
					'<h4 class="oa-contact-name">' + tch.realname + '</h4>' +
					'<span class="oa-contact-position-index-index mui-h6" style="text-align: left;">' + tch.affiliation + '</span>' +
					'</div>' +
					'<p class="oa-contact-email mui-h6" style="color:#2f9bd2;">' +
					tch.major + '-' + tch.degree +
					'</p>' +
					'<p class="mui-h6">' +
					'擅长:' +
					addCoursesHtml(tch.handledCourses) +
					'</p>' +
					'</div>' +
					'</div>' +
					'</div>';
				ul.appendChild(li);
			});
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

/**
 * 按照条件查找老师
 * @param {Object} province
 * @param {Object} university
 * @param {Object} course
 * @param {Object} sex
 */
function getSearchedTchAjax(province, university, course, sex) {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/searchTch/" + serchCount,
		data: {
			"province": province,
			"university": university,
			"sex": sex,
			"course": course
		},
		//返回按照条件查找到的老师个数，传到显示搜索出的复合条件的老师界面
		success: function(msg) {
			var data = msg;
			var ul = document.querySelector(".mui-table-view, .mui-table-view-chevron");
			var length = ul.getElementsByTagName("li").length;
			for (var i = 0; i < length; i++) {
				var li = document.querySelector(".mui-table-view-cell");
				ul.removeChild(li);
			}
			mui.each(data, function(i, tch) {
				var li = document.createElement('li');
				li.className = 'mui-table-view-cell';
				li.innerHTML = '<div onclick="openClickedTchPage(' + tch.id + ');"  class="mui-slider-cell">' + '<div class="oa-contact-cell mui-table">' +
					'<div class="oa-contact-avatar mui-table-cell">' + '<img src="' + prex + '/resources/images/tchHead/' + tch.head + '" width="60px" height="60px" style="margin-top:15px;"/>' + '</div>' +
					'<div class="oa-contact-content mui-table-cell">' +
					'<div class="mui-clearfix">' +
					'<h4 class="oa-contact-name">' + tch.realname + '</h4>' +
					'<span class="oa-contact-position-index-index mui-h6" style="text-align: left;">' + tch.affiliation + '</span>' +
					'</div>' +
					'<p class="oa-contact-email mui-h6" style="color:#2f9bd2;">' +
					tch.major + '-' + tch.degree +
					'</p>' +
					'<p class="mui-h6">' +
					'擅长:' +
					addCoursesHtml(tch.handledCourses) +
					'</p>' +
					'</div>' +
					'</div>' +
					'</div>';
				ul.appendChild(li);
			});
			serchCount = serchCount + 8; //一次加载8个
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

function addCoursesHtml(handledCourses) {
	var coursesHtml = "";
	for (j in handledCourses) {
		if (j <= 2) {
			coursesHtml = coursesHtml + '<a class="hot_tag">' + handledCourses[j] + '</a>';
		}
	}
	return coursesHtml;
}


function openClickedTchPage(id) {
	var tchId = id;
	console.info("tchId"+tchId);
	mui.openWindow({
		id: 'tchHome.html',
		url: 'tchHome.html',
		extras: {
			tchId: tchId
		},
		waiting:{
			autoShow:false
		}
	});
}