var oneActionCount = 8; //一次请求界面上显示8个

function getSearchCoursesCountAjax() {
	mui.ajax({
		type: "post",
		dataType: "json",
		data: {
			"field": field,
			"property": property,
			"agent": agent,
			"subject": subject
		},
		url: prex + "/app/getCourseCount/",
		complete: function() {},
		success: function(msg) {
			courseCount = msg;
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

/**
 * 按照筛选条件得到课程
 * 点击筛选条件触发
 * @param {Object} agent
 * @param {Object} subject
 */
function getSearchCoursesAjax() {
	ajaxCount = 0;
	document.getElementById('net-list').style.display = "none";
	document.getElementById('ftf-list').style.display = "none";
	document.getElementById('hide-box').style.display = "none";
	document.getElementById('load_model').style.display = "block";
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getSearchCourses/" + ajaxCount,
		data: {
			"field": field,
			"property": property,
			"agent": agent,
			"subject": subject
		},
		complete: function() {},
		success: function(msg) {
			document.querySelector("#course-list").innerHTML = "";
			var data = msg;
			var ul = document.querySelector("#course-list");
			if(data.length > 0){
				mui.each(data, function(i, course) {
					var li = document.createElement('li');
					li.innerHTML = '<img onclick="openClickedCoursePage(' + course.id + ');" class="course-img" src="' + 
					prex + '/resources/images/course/' + course.picPath + '" />' +
					'<div onclick="openClickedCoursePage(' + course.id + ');" class="list-div">' + '<h3>' + 
					course.courseName + '</h3>' + '<p>' + course.profile + '</p>' + '<span class="list-discount">单价：¥' + 
					course.ourPrice + '</span>' + '<span class="list-price">官网价：¥' + course.officialPrice + '</span>' + 
					'<a href="javascript:;" class="buy" onclick="addToShopCart(' + course.id + ')">' + 
					'<i class="iconfont">&#xe60c;</i>立即购买' + '</a>' + '</div>';
//					console.info(li.innerHTML);
					ul.appendChild(li);
				});
				ajaxCount = ajaxCount + 8; //每次点击筛选条件重新计数
			}else{
				ul.innerHTML = "<div style='text-align:center;'> 目前还没有相应课程,后续我们会开放哒~~ </div>";
			}
			ul = document.querySelector("#menu-list");
			document.getElementById('load_model').style.display = "none";
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

/**
 * 上拉刷新得到课程
 * @param {Object} agent
 * @param {Object} subject
 */
function getPullUpSearchCoursesAjax() {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getSearchCourses/" + ajaxCount,
		data: {
			"field": field,
			"property": property,
			"agent": agent,
			"subject": subject
		},
		complete: function() {},
		success: function(msg) {
			var data = msg;
			var ul = document.querySelector("#course-list");
			mui.each(data, function(i, course) {
				var li = document.createElement('li');
				//				li.onclick = openClickedCoursePage(course.id);
				li.innerHTML = '<img onclick="openClickedCoursePage(' + course.id + ');" class="course-img" src="' + 
				prex + '/resources/images/course/' + course.picPath + '" />' +
				'<div onclick="openClickedCoursePage(' + course.id + ');" class="list-div">' + '<h3>' + 
				course.courseName + '</h3>' + '<p>' + course.profile + '</p>' + '<span class="list-discount">单价：¥' + 
				course.ourPrice + '</span>' + '<span class="list-price">官网价：¥' + course.officialPrice + '</span>' + 
				'<a href="javascript:;" class="buy" onclick="addToShopCart(' + course.id + ')">' + 
				'<i class="iconfont">&#xe60c;</i>立即购买' + '</a>' + '</div>';
//				console.info(li.innerHTML);
				ul.appendChild(li);
			});
			ajaxCount = ajaxCount + 8; //一次加载8个
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

/**
 * 打开课程详情界面
 * @param {Object} id
 */
function openClickedCoursePage(id) {
	var courseId = id;
	console.info(courseId);
	var courseHomeId="courseHomeId"+courseId;
	mui.openWindow({
		id: courseHomeId,
		url: '/webapp/course/courseHome.html',
		extras: {
			courseId: courseId
		}
	});
}
/**
 * 用于得到前几个
 */

function getTopcourseAjax() {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getHomeCourses/" + 0,
		data: {
			"oneActionCount": oneActionCount
		},
		complete: function() {},
		success: function(msg) {
			var data = msg;
			var ul = document.querySelector(".course-list");
			mui.each(data, function(i, tch) {

			});
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}