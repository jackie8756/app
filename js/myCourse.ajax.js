//我的课程
function getOrderSize(){				
	$.ajax({
			type: "post",
			dataType: "json",
			async: false,
			url: prex + "/app/shopCart/needlogin/getOrderListSize",
			data: {
				"appAccountId": appAccountId
			},
			complete: function() {},
			success: function(data) {
			orderNumNotPaid = data.orderNumNotPaid;
			orderNumPaid = data.orderNumPaid;
			
			document.getElementById("NotPaidNum").innerHTML=orderNumNotPaid;
			document.getElementById("PaidNum").innerHTML=orderNumPaid;
			},
			failed: function(msg) {
				console.log("failed");
			}
		});
}


function getOrderListNotPaid(){
	console.info("successNotPaid");
	console.log("isPaid:"+isPaid);
	orderNumNotPaidCount = 0; //切换未支付已支付需要将序号置0
	orderNum = orderNumNotPaidCount;
//	isPaid = 0;
	mui.ajax({
			type: "post",
			dataType: "json",
			url: prex + "/app/shopCart/needlogin/getOrderList/" + orderNum,
			data: {
				"appAccountId": appAccountId,
				"isPaid": isPaid
			},
			complete: function() {},
			success: function(data) {
				shopCartlist = data.shopCart;
				var ul = document.querySelector("#item1");
				
				mui.each(shopCartlist, function(i, shopCart) {
			        var lidivInnerHtml = "";
					mui.each(shopCart.courseGoods,function(j, cs){
						var lidiv = document.createElement('div');									
						lidivInnerHtml = lidivInnerHtml +											
								'<div class="courseItem">' +
								'<span class="bookImg"><img src=" ' + prex + '/resources/images/course/' + cs.picPath + ' "></span>' +
                                '<div><p>'+ cs.courseName +'</p>' +
								'<p>￥'+ cs.ourPrice +'<span>*'+ cs.courseNum +'</span></span></p></div>' +
                                '</div>';	
//                     console.log("pic:"+ cs.picPath);           
					});
					
					var li = document.createElement('li');
					li.className = 'mui-table-view-cell';
//								console.log("shopCart.paid:"+shopCart.isPaid);
//								console.log("shopCart.orderId:"+ shopCart.orderId);
				
					li.innerHTML = lidivInnerHtml +
						'<p class="totle">订单价格:<span>￥'+ shopCart.ourTotalGoodsPrice +
						' </span>(运费0:00元) <a class="pay" onclick="repayOrder('+ shopCart.orderId + ')">去支付</a></p>';
					ul.appendChild(li);
					
//								console.log(ul.innerHTML);	
				});	
				orderNumNotPaidCount = orderNumNotPaidCount + oneActionCount;
				console.log("orderNumNotPaidCount:"+orderNumNotPaidCount);
			},
			failed: function(msg) {
				console.log("failed");
			}
		});
}


function getOrderListPaid(){
	console.info("successPaid");
	console.log("isPaid:"+isPaid);
//	isPaid = 1;
	orderNumPaidCount = 0;
	orderNum = orderNumPaidCount;
	mui.ajax({
			type: "post",
			dataType: "json",
			url: prex + "/app/shopCart/needlogin/getOrderList/" + orderNum,
			data: {
				"appAccountId": appAccountId,
				"isPaid": isPaid
			},
			complete: function() {},
			success: function(data) {
				shopCartlist = data.shopCart;				
				var ul = document.querySelector("#item3");
				
				mui.each(shopCartlist, function(i, shopCart) {
			        var lidivInnerHtml = "";
					mui.each(shopCart.courseGoods,function(j, cs){
						var lidiv = document.createElement('div');	
						console.log(cs.isComment);
						if(cs.isComment == true){
							lidivInnerHtml = lidivInnerHtml +											
								'<div class="courseItem">' +
								'<span class="bookImg"><img src=" ' + prex + '/resources/images/course/' + cs.picPath + ' "></span>' +
                                '<div><p>'+ cs.courseName +'</p>' +
                                '<p>￥'+ cs.ourPrice +'<span>*'+ cs.courseNum +'</span></span><a>已评价</a></p></div>' +
                                '</div>';	
						}else{
							lidivInnerHtml = lidivInnerHtml + 											
								'<div class="courseItem">' +
								'<span class="bookImg"><img src=" ' + prex + '/resources/images/course/' + cs.picPath + ' "></span>' +
                                '<div><p>'+ cs.courseName +'</p>' +
                                '<p>￥'+ cs.ourPrice +'<span>*'+ cs.courseNum +
                                '</span></span><a onclick="addComment('+cs.courseId + ',' + cs.id +')">去评价</a></p></div>' +
                                '</div>';	
						}										

					});
					
					var li = document.createElement('li');
					li.className = 'mui-table-view-cell';
					li.innerHTML = lidivInnerHtml +
					'<p class="totle">共计:'+ shopCart.totalGoodsNum +'件商品 &nbsp;&nbsp;合计：<span>￥'+ shopCart.ourTotalGoodsPrice +' </span>(含运费0:00元)</p>';							
														
					ul.appendChild(li);	
				});	
				
				orderNumPaidCount = orderNumPaidCount + oneActionCount;
				console.log("orderNumPaidCount:"+orderNumPaidCount);
			},
			failed: function(msg) {
				console.log("failed");
			}
		});
}


function pullToGetNotPaidOrderList(){
	console.log("orderNumNotPaidCount:"+orderNumNotPaidCount);
	orderNum = orderNumNotPaidCount;
	console.log("orderNum:"+orderNum);
	
	mui.ajax({
			type: "post",
			dataType: "json",
			url: prex + "/app/shopCart/needlogin/getOrderList/" + orderNum,
			data: {
				"appAccountId": appAccountId,
				"isPaid": isPaid
			},
			complete: function() {},
			success: function(data) {
				shopCartlist = data.shopCart;
				var ul = document.querySelector("#item1");
				
				mui.each(shopCartlist, function(i, shopCart) {
			        var lidivInnerHtml = "";
					mui.each(shopCart.courseGoods,function(j, cs){
						var lidiv = document.createElement('div');									
						lidivInnerHtml = lidivInnerHtml +											
								'<div class="courseItem">' +
								'<span class="bookImg"><img src=" ' + prex + '/resources/images/course/' + cs.picPath + ' "></span>' +
                                '<div><p>'+ cs.courseName +'</p>' +
								'<p>￥'+ cs.ourPrice +'<span>*'+ cs.courseNum +'</span></span></p></div>' +
                                '</div>';	
//                     console.log("pic:"+ cs.picPath);           
					});
					
					var li = document.createElement('li');
					li.className = 'mui-table-view-cell';
//								console.log("shopCart.paid:"+shopCart.isPaid);
//								console.log("shopCart.orderId:"+ shopCart.orderId);
				
					li.innerHTML = lidivInnerHtml +
						'<p class="totle">订单价格:<span>￥'+ shopCart.ourTotalGoodsPrice +
						' </span>(运费0:00元) <a class="pay" onclick="repayOrder('+ shopCart.orderId + ')">去支付</a></p>';
					ul.appendChild(li);
				});	
				orderNumNotPaidCount = orderNumNotPaidCount + oneActionCount;
				console.log("orderNumNotPaidCount:"+orderNumNotPaidCount);
				//self.endPullUpToRefresh();																	
			},
			failed: function(msg) {
				console.log("failed");
			}
		});
}


function pullToGetPaidOrderList(){
	orderNum = orderNumPaidCount;
	console.log("orderNum:"+orderNum);
	orderNum = orderNum + oneActionCount;
	mui.ajax({
			type: "post",
			dataType: "json",
			url: prex + "/app/shopCart/needlogin/getOrderList/" + orderNum,
			data: {
				"appAccountId": appAccountId,
				"isPaid": isPaid
			},
			complete: function() {},
			success: function(data) {
				shopCartlist = data.shopCart;				
				var ul = document.querySelector("#item3");
				
				mui.each(shopCartlist, function(i, shopCart) {
			        var lidivInnerHtml = "";
					mui.each(shopCart.courseGoods,function(j, cs){
						var lidiv = document.createElement('div');	
						console.log(cs.isComment);
						if(cs.isComment == true){
							lidivInnerHtml = lidivInnerHtml +											
								'<div class="courseItem">' +
								'<span class="bookImg"><img src=" ' + prex + '/resources/images/course/' + cs.picPath + ' "></span>' +
                                '<div><p>'+ cs.courseName +'</p>' +
                                '<p>￥'+ cs.ourPrice +'<span>*'+ cs.courseNum +'</span></span><a>已评价</a></p></div>' +
                                '</div>';	
						}else{
							lidivInnerHtml = lidivInnerHtml + 											
								'<div class="courseItem">' +
								'<span class="bookImg"><img src=" ' + prex + '/resources/images/course/' + cs.picPath + ' "></span>' +
                                '<div><p>'+ cs.courseName +'</p>' +
                                '<p>￥'+ cs.ourPrice +'<span>*'+ cs.courseNum +
                                '</span></span><a onclick="addComment('+cs.courseId + ',' + cs.id +')">去评价</a></p></div>' +
                                '</div>';	
						}										

					});
					
					var li = document.createElement('li');
					li.className = 'mui-table-view-cell';
					li.innerHTML = lidivInnerHtml +
					'<p class="totle">共计:'+ shopCart.totalGoodsNum +'件商品 &nbsp;&nbsp;合计：<span>￥'+ shopCart.ourTotalGoodsPrice +' </span>(含运费0:00元)</p>';							
														
					ul.appendChild(li);	
				});	
				orderNumPaidCount = orderNumPaidCount + oneActionCount;
				console.log("orderNumPaidCount:"+orderNumPaidCount);
			},
			failed: function(msg) {
				console.log("failed");
			}
		});
}

//得到前8条课程
/*
function getOrderListNotPaid(){
	console.info("successNotPaid");
//	orderNum = orderNumNotPaid;
	orderNum = 0;
	isPaid = 0;
	mui.ajax({
			type: "post",
			dataType: "json",
			url: prex + "/app/shopCart/needlogin/getOrderList/" + orderNum,
			data: {
				"appAccountId": appAccountId,
				"isPaid": isPaid
			},
			complete: function() {},
			success: function(data) {
				shopCartlist = data.shopCart;
				var div_orderlist = document.getElementById("orderlist");
				div_orderlist.innerHTML = '';
				
				var ul = document.createElement('ul');
				ul.className = 'mui-table-view-cell';
				ul.id = 'courseNotPaid';
				
				mui.each(shopCartlist, function(i, shopCart) {
//								console.log("ourTotalGoodsPrice:"+shopCart.ourTotalGoodsPrice);
//					var div = document.createElement('div');
//					div.id = "item1mobile" ;
//					div.className = "mui-slider-item mui-control-content mui-active";
			        var lidivInnerHtml = "";
					mui.each(shopCart.courseGoods,function(j, cs){
						var lidiv = document.createElement('div');									
						lidivInnerHtml = lidivInnerHtml +											
								'<div class="courseItem">' +
								'<span class="bookImg"><img src=" ' + prex + '/resources/images/course/' + cs.picPath + ' "></span>' +
                                '<div><p>'+ cs.courseName +'</p>' +
								'<p>￥'+ cs.ourPrice +'<span>*'+ cs.courseNum +'</span></span></p></div>' +
                                '</div>';	
//                     console.log("pic:"+ cs.picPath);           
					});
					
					var li = document.createElement('li');
					li.className = 'mui-table-view-cell';
//								console.log("shopCart.paid:"+shopCart.isPaid);
//								console.log("shopCart.orderId:"+ shopCart.orderId);
				
					li.innerHTML = lidivInnerHtml +
						'<p class="totle">订单价格:<span>￥'+ shopCart.ourTotalGoodsPrice +
						' </span>(运费0:00元) <a class="pay" onclick="repayOrder('+ shopCart.orderId + ')">去支付</a></p>';
					ul.appendChild(li);
					
//								console.log(ul.innerHTML);	
				});	
																					
					div_orderlist.innerHTML =
					'<div id="item1mobile" class="mui-slider-item mui-control-content mui-active">' +
					'<div id="scroll1" class="mui-scroll-wrapper">' +
						'<div class="mui-scroll">' +
						'<ul class="mui-table-view courseList">' +
						ul.innerHTML +
						'</ul>' +
					'</div></div></div>';	
					console.log(div_orderlist.innerHTML);
			},
			failed: function(msg) {
				console.log("failed");
			}
		});
}
*/

/*
function getOrderListPaid(){
		console.info("successPaid");
	isPaid = 1;
	orderNum = 0;
	mui.ajax({
			type: "post",
			dataType: "json",
			url: prex + "/app/shopCart/needlogin/getOrderList/" + orderNum,
			data: {
				"appAccountId": appAccountId,
				"isPaid": isPaid
			},
			complete: function() {},
			success: function(data) {
				shopCartlist = data.shopCart;
				var div_orderlist = document.getElementById("orderlist");
				div_orderlist.innerHTML = '';
				
				var ul = document.createElement('ul');
				ul.className = 'mui-table-view-cell';							
				ul.id = 'coursePaid';
				
				mui.each(shopCartlist, function(i, shopCart) {
					console.log("ourTotalGoodsPrice:"+shopCart.ourTotalGoodsPrice);
//					var div = document.createElement('div');
//					div.id = "item3mobile" ;
//					div.className = "mui-slider-item mui-control-content mui-active";
			        var lidivInnerHtml = "";
					mui.each(shopCart.courseGoods,function(j, cs){
						var innerlidivInnerHtml = "";
						var lidiv = document.createElement('div');	
						console.log(cs.isComment);
						if(cs.isComment == true){
							innerlidivInnerHtml = 											
								'<div class="courseItem">' +
								'<span class="bookImg"><img src=" ' + prex + '/resources/images/course/' + cs.picPath + ' "></span>' +
                                '<div><p>'+ cs.courseName +'</p>' +
                                '<p>￥'+ cs.ourPrice +'<span>*'+ cs.courseNum +'</span></span><a>已评价</a></p></div>' +
                                '</div>';	
						}else{
							innerlidivInnerHtml = 											
								'<div class="courseItem">' +
								'<span class="bookImg"><img src=" ' + prex + '/resources/images/course/' + cs.picPath + ' "></span>' +
                                '<div><p>'+ cs.courseName +'</p>' +
                                '<p>￥'+ cs.ourPrice +'<span>*'+ cs.courseNum +
                                '</span></span><a onclick="addComment('+cs.courseId + ',' + cs.id +')">去评价</a></p></div>' +
                                '</div>';	
						}										
                                
                        lidivInnerHtml =  lidivInnerHtml + innerlidivInnerHtml; 
                        innerlidivInnerHtml = "";
					});
					
					var li = document.createElement('li');
					li.className = 'mui-table-view-cell';
					li.innerHTML = lidivInnerHtml +
					'<p class="totle">共计:'+ shopCart.totalGoodsNum +'件商品 &nbsp;&nbsp;合计：<span>￥'+ shopCart.ourTotalGoodsPrice +' </span>(含运费0:00元)</p>';							
														
					ul.appendChild(li);	
				});	
				
				div_orderlist.innerHTML =
				'<div id="item3mobile" class="mui-slider-item mui-control-content">' +
					'<div class="mui-scroll-wrapper">' +
						'<div class="mui-scroll">' +
						'<ul class="mui-table-view courseList">' +
						ul.innerHTML +
						'</ul>' +
				'</div></div></div>';	
					console.log(div_orderlist.innerHTML);
			},
			failed: function(msg) {
				console.log("failed");
			}
		});
}
*/	
	
//静态测试
/*
function getOrderListNotPaid(){
	console.info("successNotPaid");
//	orderNum = orderNumNotPaid;
	orderNum = 0;
	isPaid = 0;
	mui.ajax({
			type: "post",
			dataType: "json",
			url: prex + "/app/shopCart/needlogin/getOrderList/" + orderNum,
			data: {
				"appAccountId": appAccountId,
				"isPaid": isPaid
			},
			complete: function() {},
			success: function(data) {
				var ul = document.getElementById("item1");
				var li = document.createElement('li');			
				li.innerHTML =	'<div class="courseItem">' +
					'<span class="bookImg"><img src="../../images/lunbo3.jpg"></span>' +
                    '<div><p>2016考研政英数全程联报班【秋季升级版】</p>' +
                    '<p>￥300.00<span>*1</span></span><a href="pinglun.html">去评价</a></p></div>' +
				'</div>' +
				'<p class="totle">共计两件商品 &nbsp;&nbsp;合计：<span>￥300.00</span>(含运费0:00元)</p>';	
				ul.appendChild(li);
				console.log(ul.innerHTML);
			},
			failed: function(msg) {
				console.log("failed");
			}
		});
}
*/





