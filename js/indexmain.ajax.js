var oneActionCount = 8; //一次请求界面上显示8个
function getTchCountAjax() {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getTchCount/",
		complete: function() {},
		success: function(msg) {
			tchCount = msg;
			//			console.log(msg);
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

function getTchAjax() {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getTch/" + ajaxCount,
		data: {
			"oneActionCount": oneActionCount
		},
		complete: function() {},
		success: function(msg) {
			var data = msg;
			var ul = document.querySelector(".miu-curose");
			mui.each(data, function(i, tch) {
				var li = document.createElement('li');
				li.className = 'mui-table-view mui-grid-view miu-curose-list';
				li.innerHTML = '<div onclick="openClickedTchPage(' + tch.id + ');"  class="mui-slider-cell">' + '<div class="oa-contact-cell mui-table">' +
					'<div class="oa-contact-avatar mui-table-cell">' + '<img src="' + prex + '/resources/images/tchHead/' + tch.head + '" width="55" height="55"  style="margin-left: 10px;"/>' + '</div>' +
					'<div class="oa-contact-content mui-table-cell">' +
					'<div class="mui-clearfix">' +
					'<h4 class="oa-contact-name"style="width: 100%;margin: 15px 0 10px 0;">' + tch.realname +
					'<span class=" mui-h6" style="float: right;font-size: 14px;margin:0 10px 0 0;" >' + tch.affiliation + '</span>' + '</h4>' +
					'</div>' +
					'<p class="oa-contact-email mui-h6" style="color:#2f9bd2;margin-bottom:0;">' +
					tch.major + '-' + tch.degree +
					'</p>' +
					'</div>' +
					'</div>' +
					'</div>';
				ul.appendChild(li);
				document.getElementById('load_model').style.display = "none";
			});

			ajaxCount = ajaxCount + 8; //一次加载8个
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

/**
 * 用于得到前几个
 */
function getTopTchsAjax() {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/getTch/" + 0,
		data: {
			"oneActionCount": oneActionCount
		},
		complete: function() {},
		success: function(msg) {
			var data = msg;
			var ul = document.querySelector(".miu-curose");
			mui.each(data, function(i, tch) {
				var li = document.querySelector(".miu-curose-list");
//				console.info(li);
				if(li != null){
					ul.removeChild(li); //每次都删除第一个
				}
				li = document.createElement('li');
				li.className = 'mui-table-view mui-grid-view miu-curose-list';
				li.innerHTML = '<div onclick="openClickedTchPage(' + tch.id + ');"  class="mui-slider-cell">' + '<div class="oa-contact-cell mui-table">' +
					'<div class="oa-contact-avatar mui-table-cell">' + '<img src="' + prex + '/resources/images/tchHead/' + tch.head + '" width="55" height="55"  style="margin-left: 10px;"/>' + '</div>' +
					'<div class="oa-contact-content mui-table-cell">' +
					'<div class="mui-clearfix">' +
					'<h4 class="oa-contact-name"style="width: 100%;margin: 15px 0 10px 0;">' + tch.realname +
					'<span class=" mui-h6" style="float: right;font-size: 14px;margin:0 10px 0 0;" >' + tch.affiliation + '</span>' + '</h4>' +
					'</div>' +
					'<p class="oa-contact-email mui-h6" style="color:#2f9bd2;margin-bottom:0;">' +
					tch.major + '-' + tch.degree +
					'</p>' +
					'</div>' +
					'</div>' +
					'</div>';
				ul.appendChild(li);
				document.getElementById('load_model').style.display = "none";
			});
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

/**
 * 按照条件查找老师
 * @param {Object} province
 * @param {Object} university
 * @param {Object} course
 * @param {Object} sex
 */
function getSearchedTchAjax(province, university, course, sex) {
	mui.ajax({
		type: "post",
		dataType: "json",
		url: prex + "/app/searchTch/" + serchCount,
		data: {
			"province": province,
			"university": university,
			"sex": sex,
			"course": course
		},
		//返回按照条件查找到的老师个数，传到显示搜索出的复合条件的老师界面
		success: function(msg) {
			var data = msg;
			var ul = document.querySelector(".miu-curose");
			var length = ul.getElementsByTagName("li").length;
			for (var i = 0; i < length; i++) {
				var li = document.querySelector(".miu-curose-list");
				ul.removeChild(li);
			}
			mui.each(data, function(i, tch) {
				var li = document.createElement('li');
				li.className = 'mui-table-view mui-grid-view miu-curose-list';
				li.innerHTML = '<div onclick="openClickedTchPage(' + tch.id + ');"  class="mui-slider-cell">' + '<div class="oa-contact-cell mui-table">' +
					'<div class="oa-contact-avatar mui-table-cell">' + '<img src="' + prex + '/resources/images/tchHead/' + tch.head + '" width="55" height="55"  style="margin-left: 10px;"/>' + '</div>' +
					'<div class="oa-contact-content mui-table-cell">' +
					'<div class="mui-clearfix">' +
					'<h4 class="oa-contact-name"style="width: 100%;margin: 15px 0 10px 0;">' + tch.realname +
					'<span class=" mui-h6" style="float: right;font-size: 14px;margin:0 10px 0 0;" >' + tch.affiliation + '</span>' + '</h4>' +
					'</div>' +
					'<p class="oa-contact-email mui-h6" style="color:#2f9bd2;margin-bottom:0;">' +
					tch.major + '-' + tch.degree +
					'</p>' +
					'</div>' +
					'</div>' +
					'</div>';
				ul.appendChild(li);
			});
			serchCount = serchCount + 8; //一次加载8个
		},
		failed: function(msg) {
			console.log("failed");
		}
	});
}

function addCoursesHtml(handledCourses) {
	var coursesHtml = "";
	for (j in handledCourses) {
		if (j <= 2) {
			coursesHtml = coursesHtml + '<a class="hot_tag">' + handledCourses[j] + '</a>';
		}
	}
	return coursesHtml;
}


function openClickedTchPage(id) {
	var tchId = id;
	console.info("tch id:"+ tchId);
	mui.openWindow({
		id: 'tchHome.html',
		url: '/webapp/tutor/tchHome.html',
		extras: {
			tchId: tchId
		},
		waiting:{
			autoShow:false
		}
	});
}